package roomquest.cse.csusb.edu.roomquest;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;



/**
 * Author: Christopher Koenig
 * File:About.java
 * Date:10/9/17.
 * Description:This is the about the screen of the app.
 */

public class About extends DialogFragment{


    //View one, two, three;//grid buttons

    //Communicator communicator;
    //int gridBoxNum = 0 ; //this will be passed to main activity

    /*@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        communicator = (Communicator) activity;
    }*/

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View  view = inflater.inflate(R.layout.about,null);
        //set title on dialogFrag
        //getDialog().setTitle("Menu");
        //getDialog().setTitle(Html.fromHtml("<font color = '#ffffff'> Menu </font>"));
        //int dividerId = getDialog().getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
        //View divider = getDialog().findViewById(dividerId);
        //divider.setBackgroundColor(getResources().getColor(R.color.justWhite));

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));




        setCancelable(true);//prevents user from canceling the about screen

        return view; //inflater.inflate(R.layout.about, null);
    }

    /*public void onClick(View view){

        if(view.getId() == R.id.grid_button_one){
            communicator.onDialogMessage(1);
            dismiss();
        }
        else if(view.getId() == R.id.grid_button_two){
            communicator.onDialogMessage(2);
            dismiss();
        }else if(view.getId() == R.id.grid_button_three){
            communicator.onDialogMessage(3);
            dismiss();
        }
    }*/
    /*interface  Communicator {
        public void onDialogMessage(int boxNum);
    }*/

}//END CLASS ABOUT
